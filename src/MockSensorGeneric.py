import json


class MockSensorGeneric:
    def __init__(self, source_file: str):
        with open(source_file) as file:
            self.measurements = iter(json.load(file))

    def __iter__(self):
        return self

    def __next__(self):
        return json.dumps(next(self.measurements))


if __name__ == '__main__':
    sensor = MockSensorGeneric('../meta/voltage.json')
    for reading in sensor:
        print(reading)